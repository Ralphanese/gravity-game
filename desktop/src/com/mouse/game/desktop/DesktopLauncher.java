package com.mouse.game.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mouse.game.MouseGame;

public class DesktopLauncher {
	float delta = Gdx.graphics.getDeltaTime();

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new MouseGame(), config);
		config.width = MouseGame.V_WIDTH;
		config.height = MouseGame.V_HEIGHT;
		config.title = "MegaGame v0.1.4";
	}
}
