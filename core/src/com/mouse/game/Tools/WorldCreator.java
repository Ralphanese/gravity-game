package com.mouse.game.Tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.*;
import com.mouse.game.MouseGame;
import com.mouse.game.Screens.PlayScreen;

public class WorldCreator {

    PlayScreen screen;
    float centerWidth;
    float centerHeight;


    public WorldCreator(PlayScreen screen){
        this.screen = screen;
        World world = screen.getWorld();
        TiledMap map = screen.getMap();

        //Box2D collision stuff

        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;




        //ground collision
        for(MapObject object1 : map.getLayers().get(1).getObjects().getByType(RectangleMapObject.class)){
            Rectangle collisionRect = ((RectangleMapObject)object1).getRectangle();

            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((collisionRect.getX() + collisionRect.getWidth()/2)/ MouseGame.PPM, (collisionRect.getY() + collisionRect.getHeight()/2)/ MouseGame.PPM);

            body = world.createBody(bdef);

            shape.setAsBox(collisionRect.getWidth()/2 / MouseGame.PPM, collisionRect.getHeight()/2 /MouseGame.PPM);

            fdef.shape = shape;
            body.createFixture(fdef);
        }
        //camera placement

        for (MapObject object2 : map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)){
            Rectangle cameraRect = ((RectangleMapObject)object2).getRectangle();

            centerWidth = (cameraRect.getX() + (cameraRect.getWidth()/2)) / MouseGame.PPM;
                    //((cameraRect.getX() + cameraRect.getWidth())/2)/MouseGame.PPM;
            centerHeight = (cameraRect.getY() + (cameraRect.getHeight()/2) )/ MouseGame.PPM;
                    //((cameraRect.getY() + cameraRect.getHeight())/2)/MouseGame.PPM;
        }


    }
    public float getCenterWidth() {
        return centerWidth;
    }

    public float getCenterHeight() {
        return centerHeight;
    }


}
