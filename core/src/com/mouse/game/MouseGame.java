package com.mouse.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mouse.game.Screens.PlayScreen;


public class MouseGame extends Game {

	public static final int V_WIDTH = 1280;
	public static final int V_HEIGHT = 720;
	public static final float PPM = 50;

	public SpriteBatch batch;
	public PlayScreen playScreen;

	public static final short NOTHING_BIT = 0;
	public static final short GROUND_BIT = 1;
	public static final short PLAYER_BIT = 2;



	
	@Override
	public void create () {
		batch = new SpriteBatch();
		playScreen = new PlayScreen(this);

		setScreen(playScreen);


	}

	@Override
	public void render () {
		super.render();

	}
}
