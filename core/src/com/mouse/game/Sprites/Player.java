package com.mouse.game.Sprites;

import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.physics.box2d.*;
import com.mouse.game.MouseGame;
import com.mouse.game.Screens.PlayScreen;

public class Player extends Sprite {

    //imports
    public MouseGame game;
    public PlayScreen screen;
    public World world;

    //player stuff
  //  private enum State{GROUNDED, FLOATING, DEAD};

   // private State currentState;
    //private State previousState;




    public Body playerBody;
    BodyDef bdef;
    FixtureDef fdef1;

    CircleShape shape1;



    //number values
    private float MAX_JUMP = 10;
    private float circleRadius = 26;
    private float stateTimer;




    public Player(PlayScreen screen, World world){


        this.screen = screen;
        this.world = world;

        boolean isGrounded = false;

        playerDef();

    }

    public void playerDef(){

        bdef = new BodyDef();
        bdef.position.set(8, 66);
        bdef.type = BodyDef.BodyType.DynamicBody;
        playerBody = world.createBody(bdef);

        fdef1 = new FixtureDef();
        shape1 = new CircleShape();
        fdef1.shape = shape1;
        fdef1.friction = 2.5f;
        shape1.setRadius(circleRadius / MouseGame.PPM);
        fdef1.filter.categoryBits = MouseGame.PLAYER_BIT;
        fdef1.filter.maskBits = MouseGame.GROUND_BIT;



        playerBody.createFixture(fdef1).setUserData(this);



    }

    public void update(float dt) {
        setPosition(playerBody.getPosition().x - getWidth() / 2, playerBody.getPosition().y - getHeight() / 2);



    }
    public void draw (SpriteBatch batch){



    }


    public void dispose(){

        shape1.dispose();

    }

    public void HandleInput(float dt){




    }




}
