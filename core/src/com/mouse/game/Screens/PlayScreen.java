package com.mouse.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mouse.game.MouseGame;
import com.mouse.game.Sprites.Player;
import com.mouse.game.Tools.WorldContactListener;
import com.mouse.game.Tools.WorldCreator;


public class PlayScreen implements Screen {


   MouseGame game;
    public World world;
    public Player player;
    public OrthographicCamera playCam;
    public OrthogonalTiledMapRenderer mapRenderer;
    public TiledMap map;
    public Viewport gameView;
    public Box2DDebugRenderer debug;
    public WorldCreator creator;
    public SpriteBatch batch;
    public WorldContactListener listener;




    public PlayScreen(MouseGame game){

        this.game = game;
        playCam = new OrthographicCamera();
        gameView = new StretchViewport(1280/game.PPM, 720/game.PPM, playCam);
        world = new World(new Vector2(0, -.25f), true);
        debug = new Box2DDebugRenderer();
        map = new TmxMapLoader().load("MegaMap2.tmx");
        mapRenderer = new OrthogonalTiledMapRenderer(map, 1/game.PPM);
        creator = new WorldCreator(this);
        listener = new WorldContactListener();
        batch = new SpriteBatch();

        player = new Player(this, world);
        world.setContactListener(listener);

        playCam.setToOrtho(false,1280, 720);

    }

    public void handleCamera(float delta){
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))playCam.position.add(5/MouseGame.PPM, 0, 0);
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT))playCam.position.add(-5/MouseGame.PPM, 0, 0);
        if (Gdx.input.isKeyPressed(Input.Keys.UP))playCam.position.add(0, 5/MouseGame.PPM, 0);
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN))playCam.position.add(0, -5/MouseGame.PPM, 0);



    }

    public void update(float delta){
        handleCamera(delta);
        player.HandleInput(delta);
        playCam.position.set(creator.getCenterWidth(), creator.getCenterHeight(), 0);
        player.update(delta);

        world.step(1/60f, 6, 2);



        playCam.update();
        mapRenderer.setView(playCam);




    }



    @Override
    public void show() {

    }



    @Override
    public void render(float delta) {

        update(delta);

        Gdx.gl20.glClearColor(0, 0, 0 , 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);


        mapRenderer.render();


        debug.render(world, playCam.combined);



        System.out.println();

        batch.setProjectionMatrix(playCam.combined);
        batch.begin();
        player.draw(batch);
        batch.end();




    }

    @Override
    public void resize(int width, int height) {

        gameView.update(width, height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        world.dispose();
        player.dispose();
        map.dispose();


    }
    public World getWorld() {
        return world;
    }

    public Player getPlayer() {
        return player;
    }

    public OrthographicCamera getPlayCam() {
        return playCam;
    }


    public TiledMap getMap() {
        return map;
    }
}
